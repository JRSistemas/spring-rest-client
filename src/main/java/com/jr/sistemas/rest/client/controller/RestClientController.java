package com.jr.sistemas.rest.client.controller;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.jr.sistemas.rest.client.domain.Person;
import com.jr.sistemas.rest.client.domain.PersonList;
import com.jr.sistemas.rest.util.Writer;

@Controller
public class RestClientController 
{
	private static Logger logger=Logger.getLogger("controller");
	// El RestTemplate , permite llamar al otro servicio
	private RestTemplate restTemplate=new RestTemplate();
	//Model es una interface donde se puede poner objetos que despues se pueden poner en la vista
	
	@RequestMapping(value="/getall", method=RequestMethod.GET)
	public String getAll(Model model)
	{
		logger.debug("Recuperar todas las personas de mi agenda");
		// Preparamos los tipos de datos a trabajar
		List<MediaType> acceptableMediaTypes=new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_XML);
		// Preparamos el headers
		
		HttpHeaders headers=new HttpHeaders();
		headers.setAccept(acceptableMediaTypes);
		HttpEntity<Person> entity=new HttpEntity<Person>(headers);
		
		//Enviamos el request via GET
		try {
		ResponseEntity<PersonList> result=restTemplate.exchange("http://localhost:8080/spring-rest-server/agenda/persons",
							 HttpMethod.GET, entity , PersonList.class);
		
		model.addAttribute("persons",result.getBody().getPersonas());
		}catch (Exception e) {
			logger.error(e);
		}
		return "personPage";
	}
	
	/*
	@RequestMapping(value="/getPhoto", method=RequestMethod.GET)
	public void getPhoto(@RequestParam("id") Long id, HttpServletResponse response)
	{
	logger.debug("Recibimos la foto con id: "+id);
	List<MediaType> acceptableMediaType=new ArrayList<MediaType>();
	acceptableMediaType.add(MediaType.IMAGE_JPEG);
	//Preparamos el hearder
	HttpHeaders headers=new HttpHeaders();
	headers.setAccept(acceptableMediaType);
	HttpEntity<String> entity=new HttpEntity<String>(headers);
	//enviamos la solicitud como obtener
	ResponseEntity<byte[]> result=restTemplate.exchange("http://localhost:8080/spring-rest/provider/kram", method, requestEntity, responseType)
			
	Writer.write(response, result.getBody());		
	}
	*/
	
}
