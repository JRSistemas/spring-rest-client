package com.jr.sistemas.rest.client.domain;

import javax.xml.bind.annotation.XmlRootElement;

// Nombre para que se muestre en el XML
@XmlRootElement(name="person")
public class Person 
{
	private Long id;
	private String firstName;
	private String lastName;
	private Double money;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", moeny=" + money + "]";
	}
	
	
	
}
