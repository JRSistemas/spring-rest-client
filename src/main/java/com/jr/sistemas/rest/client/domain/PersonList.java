package com.jr.sistemas.rest.client.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/*
 * Clase que me representa una lista de personas
 */
@XmlRootElement(name="persons")
public class PersonList 
{
	public List<Person> data;

	public List<Person> getPersonas() {
		return this.data;
	}

	public void setData(List<Person> valor) {
		this.data = valor;
	}


}
