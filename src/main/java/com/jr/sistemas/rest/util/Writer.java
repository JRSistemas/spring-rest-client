package com.jr.sistemas.rest.util;

import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class Writer 
{
	private static Logger logger=Logger.getLogger("service");
	
	public static void write(HttpServletResponse response, ByteArrayOutputStream bao)
	{
		logger.debug("Escribir un reporte al stream");
		try {
			
			ServletOutputStream outputStream=response.getOutputStream();
			bao.writeTo(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			logger.error("No se puede escribir la salida al Stream");
		}
	}
	
	public static void write(HttpServletResponse response, byte[] byteArray)
	{
		logger.debug("Escribir un reporte al stream");
		try {
			
			ServletOutputStream outputStream=response.getOutputStream();
			outputStream.write(byteArray);
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			logger.error("No se puede escribir la salida al Stream");
		}
	}
}
