<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Persons</h1>
	<c:url var="addUrl" value="/agenda/add"/>
	<table style="border: 1px solid; width: 500px; text-align: center;">
		<thead style="background: #fcf">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Money</th>
				<th colspan="4"></th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${persons}" var="person">
				<c:url var="editUrl" value="/agenda/update?id=${person.id}"/>
				<c:url var="deleteUrl" value="/agenda/delete?id=${person.id}"/>
				<c:url var="getUrl" value="/agenda/get?id=${person.id}"/>
				<tr>
					<td><c:out value="${person.firstName}"/></td>
					<td><c:out value="${person.firstName}"/></td>
					<td><c:out value="${person.money}"/></td>
					<td><a href="${editUrl}">Edit</a></td>
					<td><a href="${deleteUrl}">Delete</a></td>
					<td><a href="${eddUrl}">Add</a></td>
					<td><a href="${getUrl}">Get</a></td>
					
				</tr>
			</c:forEach>
		</tbody>
		
	
	</table>
	<c:if test="${empty persons}">
	Actualmente no hay personas en la lista. <a href="${addUrl}">Add</a> una persona  
	</c:if>
	
</body>
</html>